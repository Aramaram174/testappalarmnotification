package com.example.asus_pc.testaplication;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import static com.example.asus_pc.testaplication.App.count;
import static com.example.asus_pc.testaplication.App.name;


public class MyService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        name = "Android";
    }

//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        return onStartCommand(intent, flags, startId);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void task () {
        String s1 = "Android";
        String s2 = "iOS";
        if (count == 1){
            name = s1;
            count = 2;
        }else {
            name = s2;
            count = 1;
        }
        Intent intent = new Intent(this, TwoActivity.class);
        startActivity(intent);
    }
}
